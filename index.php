<?php

  require_once('error.php');

  $errors = array();
  $messages = array();

  if(isset($_POST['enviar'])){
    // VALIDATION CHECKS

    // Nombre y apellido
    $errors['nombreapellido'] = array();

    if ( validate_length($_POST['nombreapellido'], 0, 40) ) {
      $errors['nombreapellido'][] = "Superaste los 40 caracteres.";
    };

    // E-mail
    $errors['email'] = array();

    if (empty($_POST['email'])) {
      $errors['email'][] = "Campo vacío.";
    }
    elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
      $errors['email'][] = "Invalido.";
    };

    // Asunto
    $errors['asunto'] = array();

    if (empty($_POST['asunto'])) {
      $errors['asunto'][] = "Campo vacío.";
    }
    elseif ( validate_length($_POST['asunto'], 4, 100) ) {
      $errors['asunto'][] = "Ingrese por favor entre 4 y 100 caracteres.";
    };

    // Mensaje
    $errors['mensaje'] = array();

    if (empty($_POST['mensaje'])) {
      $errors['mensaje'][] = "Campo vacío.";
    }
    elseif ( validate_length($_POST['mensaje'], 10, 1000) ) {
      $errors['mensaje'][] = "Ingrese por favor entre 10 y 1000 caracteres.";
    };

    // Check if has been an error. If everything is ok, do the thing.
    $ok = true;
    foreach ($errors as $error) {
      if($error) {
        $ok = false;
        break;
      }
    }

    if($ok){
      $email_to = "imprentasegarra@gmail.com";
      $email_subject = "Contacto desde la web";
      $email_message = "Detalles del contacto:\n\n";
      $email_message .= "Nombre y Apellido: " . $_POST['nombreapellido'] . "\n";
      $email_message .= "E-mail: " . $_POST['email'] . "\n";
      $email_message .= "Asunto: " . $_POST['asunto'] . "\n";
      $email_message .= "Mensaje: " . $_POST['mensaje'] . "\n\n";

      $headers = 'Para: '.$_POST['email']."\r\n".
      'Reply-To: '.$_POST['email']."\r\n" .
      'X-Mailer: PHP/' . phpversion();
      if(mail($email_to, $email_subject, $email_message, $headers)){
        $messages['mail'] = array();
        $messages['mail'][] = '¡El formulario se fue enviado correctamente!';
        unset($_POST['nombreapellido']);
        unset($_POST['email']);
        unset($_POST['asunto']);
        unset($_POST['mensaje']);
        unset($_POST['enviar']);
      }else{
        $errors['mail'] = array();
        $errors['mail'][] = 'Error al enviar el mensaje!';
      }
    }
  }
?>

<!DOCTYPE HTML>
<html lang="es">
	<head>
		<title>Imprenta Segarra</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimun-scale=1" />
		<link rel="shortcut icon" href="principal/imagenes/" />
		<link href='https://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="principal/estilos/base.css" />
	</head>
	<body>
		<div class="contenedor">

			<!-- Cabecera -->
			
				<header id="cabecera">

					<!-- Logo -->
					<div class="contenedor">
						<a class="logo" href="#">
							<img src="principal/imagenes/logo.svg" alt="Logo de Imprenta Segarra" />
						</a>
						<!-- Navegador movil -->
						<a class="nav-movil" id="toggle-menu" href="#">
							<img src="principal/imagenes/navegador_movil.svg" alt="" />
						</a>
					</div>

					<!-- Navegador -->
					<nav id="menu">
						<!-- Navegador no movil -->
						<ul>
							<li class="activo"><a href="#inicio">INICIO</a></li>
							<li><a href="#productos">PRODUCTOS</a></li>
							<li><a href="#quienessomos">QUIENES SOMOS</a></li>
							<li><a href="#contacto">CONTACTO</a></li>
							<li><a href="#info">INFORMACIÓN</a></li>
							<li><a href="#mapa">MAPA</a></li>
							
						</ul>
					</nav>

				</header>

			<!-- Inicio -->

				<section id="inicio">
					<hgroup>
						<h1>Imprenta Segarra</h1>
						<p>SU IMPRENTA AMIGA</p>
					</hgroup>
					<div id="contenedor">
						<a class="facebook" href="https://web.facebook.com/Imprenta-Segarra-398081367054781" target="_blank"><img src="principal/imagenes/facebook.svg" alt="Logo de Facebook" /></a>
						<a class="twitter" href="https://twitter.com/imprentasegarra" target="_blank"><img src="principal/imagenes/twitter.svg" alt="Logo de Twitter" /></a>
						<a class="googleplus" href="https://plus.google.com/u/0/114390076146162310029" target="_blank"><img src="principal/imagenes/google_plus.svg" alt="Logo de Google Plus" /></a>
					</div>
				</section>

		 	<!-- Productos -->

				<section id="productos">
					<hgroup>
						<h2>PRODUCTOS</h2>
						<p>No vendemos productos, sino satisfacción al cliente. Óptimos presupuestos en en alta y bajas tiradas. Muchos productos a precios competitivos con múltiples opciones. Le ahorramos dinero con nuestros precios asequibles, pero le podemos ahorrar tiempo, también. Estamos tan seguros de que usted nos encantará que ofrecemos una garantía del 100% la satisfacción del cliente.</p>
					</hgroup>
					<div class="contenedor">
						<article>
							<img src="principal/imagenes/producto1.jpg" alt="Tarjetas Personales" />
							<h3>TARJETAS PERSONALES</h3>
						</article>
						<article>
							<img src="principal/imagenes/producto2.jpg" alt="Folletos" />
							<h3>FOLLETOS</h3>
						</article>
						<article>
							<img src="principal/imagenes/producto3.jpg" alt="Trabajos comerciales" />
							<h3>TRABAJOS COMERCIALES</h3>
						</article>
						<article>
							<img src="principal/imagenes/producto_sublimacion.jpg" alt="Sublimación" />
							<h3>SUBLIMACIÓN</h3>
						</article>
						<article>
							<img src="principal/imagenes/producto6.jpg" alt="Tickets para básculas" />
							<h3>TICKETS PARA BÁSCULAS</h3>
						</article>
					</div>
				</section>

			<!-- Quienes Somos -->

				<section id="quienessomos">

					<hgroup>
						<h2>QUIENES SOMOS</h2>
						<p>Una familia de gráficos que comenzó con Enrique e Irma en el año 1968 fundando Imprenta Segarra en la ciudad de Casilda, luego siguió siendo atendida por sus hijos Eduardo y Juan, y desde ahora por sus nietos Lisandro y Facundo. Cuenta con distintos tipos de servicios actualizados a las tecnologías de hoy y las necesidades del cliente. Estos 47 años de servicios muestran nuestra experiencia al servicio de ustedes.</p>
					</hgroup>
					<div>
						<article>
							<img src="principal/imagenes/perfil_eduardo.jpg" alt="perfil_eduardo" />
							<h3>Segarra Eduardo Enrique</h3>
						</article>
						<article>
							<img src="principal/imagenes/perfil_lisandro.jpg" alt="perfil_lisandro" />
							<h3>Segarra Marcos Lisandro </h3>
						</article>
						<article>
							<img src="principal/imagenes/perfil_facundo.jpg" alt="perfil_facundo" />
							<h3>Segarra Facundo Roman</h3>
						</article>
					</div>
						</header>
					</section>

			<!-- Contacto -->

				<section id="contacto">

					<hgroup>
						<h2>CONTACTO</h2>
						<p>Envienos su consulta y le responderemos en brevedad.</p>
					</hgroup>

					<!-- Formulario de contacto -->
				    
				    <?php errors_for_field($errors, 'mail') ?>
        			<?php messages_for_field($messages, 'mail') ?>

				    <form action="#contacto" method="POST">

				    	<p>Nombre y apellido</p>
				      	<input name="nombreapellido" id="nombreapellido"type="text" placeholder="Nombre y apellido" value="<?php value_field('nombreapellido') ?>" />
				      	<?php errors_for_field($errors, 'nombreapellido') ?>

				      	<p>Email *</p>
				        <input name="email" id="email" type="text" placeholder="Email" value="<?php value_field('email') ?>" requerid />
				       	<?php errors_for_field($errors, 'email') ?>

				        <p>Asunto *</p>
				        <input name="asunto" id="asunto" type="text" placeholder="Asunto" value="<?php value_field('asunto') ?>" requerid />
				        <?php errors_for_field($errors, 'asunto') ?>

				      	<p>Mensaje *</p>
				       	<textarea name="mensaje" requerid><?php value_field('mensaje') ?></textarea>
				       	<?php errors_for_field($errors, 'mensaje') ?>

				       	<input name="enviar" id="enviar" type="submit" value="Enviar" />

				    </form>

				</section>

			<!-- Información -->
				<section id="info">

					<ul>

						<li>
							<img src="principal/imagenes/localizacion.svg" alt="Ubicación" />
							<p>Av. Ovidio Lagos y Catamarca • Casilda • Santa Fe • CP2170</p>
						</li>

						<li>
							<img src="principal/imagenes/email.svg" alt="Email" />
							<p>imprentasegarra@gmail.com</p>
						</li>

						<li>
							<img src="principal/imagenes/telefono.svg" alt="Teléfono" />
							<p>Tel. (03464) 427279</p>
						</li>

					</ul>

				</section>

			<!-- Mapa -->
				<section id="mapa">
					<hgroup>
						<h2>UBICACIÓN</h2>
					</hgroup>
					<iframe frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Boulevard%20Ovidio%20Lagos%20y%20Catamarca%20Casilda%20Santa%20Fe&key=AIzaSyCTiF_rU117cirna6cA420eFHIQsjFD_VU" allowfullscreen></iframe>	
				</section>
			
			<!-- Pie -->
				<footer id="pie">

					<ul>
						<li>
							Diseñado por FRS.
						</li>
					</ul>

				</footer>

			<!-- Scripts -->
			<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
			<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
			<script type="text/javascript" src="principal/scripts/despliegue_menu.js"></script>
		</div>
	</body>
</html>
